########################################
############  DESCRIPTION   ############
########################################


# This R script aims to model topics



########################################
############  REQUIREMENT   ############
########################################


require('tidyverse')
require('topicmodels')
require('quanteda')



########################################
##########  PRE-PROCESSING   ###########
########################################


# create a matrix of only abstracts and titles
econlit.abstract <- data_frame(line=1:length(econlit_merged$abstract), title = econlit_merged$title,  text = econlit_merged$abstract)

# create dfm object
dfm.abstract <- texts(econlit.abstract$text) %>%
  tokens() %>% 
  tokens_tolower() %>%
  tokens_wordstem() %>%
  tokens_remove(stopwords("english")) %>% 
  dfm()

# convert dfm object to dtm object to be used with topicmodels
dtm.abstract <- convert(dfm.abstract, to="topicmodels")

# create model
lda_model <- LDA(x = dtm.abstract, k = 10, control = list(seed = 2811))

# get beta matrix
lda_model_beta <- tidy(lda_model, matrix = "beta")

# plot the 10 topics 
lda_model_beta %>%
  group_by(topic) %>%
  top_n(10, beta) %>%
  ungroup() %>%
  arrange(topic, -beta) %>%
  ggplot(aes(reorder(term, beta), beta, fill = factor(topic))) +
  geom_col(show.legend = FALSE) +
  facet_wrap(~ topic, scales = "free") +
  scale_fill_viridis_d() + 
  coord_flip() + 
  labs(x = "Topic", 
       y = "beta score", 
       title = "Topic modeling of econlit 'economics' database")
